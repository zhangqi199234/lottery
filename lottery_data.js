var lottery_initial_datas =[
    {
        "nameen": "avatar1",
        "namezh": "杨燕瑕",
        "wish": "人间值得，未来可期！"
        },
        {
        "nameen": "avatar2",
        "namezh": "夏先荣",
        "wish": "轻声问候、真挚祝福，愿快乐常伴你左右，愿你拥有满怀的欢欣、丰收的希望，洋溢在这新的一年!"
        },
        {
        "nameen": "avatar3",
        "namezh": "杜欢欢",
        "wish": "祝公司越来越好，新年新气象，财源滚滚来！"
        },
        {
        "nameen": "avatar4",
        "namezh": "刘洋",
        "wish": "祝大家新年身体健康，阖家辛福！"
        },
        {
        "nameen": "avatar5",
        "namezh": "廖雪冬",
        "wish": "新年发大财，买彩票中两千万！"
        },
        {
        "nameen": "avatar6",
        "namezh": "肖驰",
        "wish": "新年快乐，万事如意"
        },
        {
        "nameen": "avatar7",
        "namezh": "李跃",
        "wish": "抽奖必中，躺着赚钱！"
        },
        {
        "nameen": "avatar8",
        "namezh": "胡益铭",
        "wish": "鼠年心情好，天天没烦恼！"
        },
        {
        "nameen": "avatar9",
        "namezh": "王思婕",
        "wish": "愿2020的我们，只争朝夕，不负韶华！"
        },
        {
        "nameen": "avatar10",
        "namezh": "蒋芳",
        "wish": "愿大家一年365天天天开心，8760小时时时快乐，5256000分分分精彩，31536000秒秒秒幸福！"
        },
        {
        "nameen": "avatar11",
        "namezh": "许正祥",
        "wish": "2020年，拥抱变化，迎接挑战，望工作更上一层楼，祝创匠越做越大，越做越强！"
        },
        {
        "nameen": "avatar12",
        "namezh": "林超",
        "wish": "不忘初心，携手前行，辞旧迎新，共创未来，祝公司在新的一年红红火火，蒸蒸日上！"
        },
        {
        "nameen": "avatar13",
        "namezh": "卢峰",
        "wish": "鼠年大吉，财源广进。"
        },
        {
        "nameen": "avatar14",
        "namezh": "张泽东",
        "wish": "新年快乐，鼠年吉祥，事事顺利，天天开心，健康最重要，新的一年发大财！"
        },
        {
        "nameen": "avatar15",
        "namezh": "张琦",
        "wish": "不忘初心，砥砺前行！"
        },
        {
        "nameen": "avatar16",
        "namezh": "杜永雄",
        "wish": "新春将至，希望我的朋友都是幸福的，快乐的，没有烦恼，没有委屈，爱自己，幸福跟着来，温馨过新春！"
        },
        {
        "nameen": "avatar17",
        "namezh": "程燕明",
        "wish": "机缘，在合作中生根，情谊，在合作中加深;事业在合作中壮大;梦想，在合作中腾飞。愿新的一年，我们继续再接再厉，合作双赢，创造新的辉煌！"
        },
        {
        "nameen": "avatar18",
        "namezh": "罗姗",
        "wish": "一片好运来，两串爆竹响，散发好运气，四方来财气，五福升高空，六六都顺心，齐来迎新年，八方收和气，久久享福祉。祝新的一年，十全十美，新年快乐！"
        }
];

var award_config = {
    "award01": 1,
    "award02": 1,
    "award03": 2,
    "award04": 3
};

// 初始化数据
if (!localStorage.getItem('lottery_initial')) {
    var data_str = JSON.stringify(lottery_initial_datas);
    localStorage.setItem('lottery_initial', data_str);
}
if (!localStorage.getItem('award_initial')) {
    var award_str = JSON.stringify(award_config);
    localStorage.setItem('award_initial', award_str);
}